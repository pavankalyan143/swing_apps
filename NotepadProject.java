package MyProjects;
//import java.awt.FlowLayout;
import java.awt.Image;
import javax.swing.*;    
import java.awt.event.*;    
import static java.awt.image.ImageObserver.HEIGHT;
import java.io.*;    
import java.util.Date;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
//import javax.swing.event.CaretListener;
public class NotepadProject extends JFrame implements ActionListener{
NotepadProject om;    
JMenuBar mb;    
JMenu file,edit,format,view,help;    
JMenuItem open,New,save,save_as,page_setup,print,exit,undo,cut,copy,paste,find,replace,selectAll,delete,td,Goto;    
JTextArea ta;    
JFrame fr=new JFrame();
JSeparator sep=new JSeparator();
JSeparator sep1=new JSeparator();
JSeparator sep2=new JSeparator();
JSeparator sep3=new JSeparator();
JSeparator sep4=new JSeparator();
JFileChooser fc=new JFileChooser(); 

NotepadProject(){ 
open=new JMenuItem("Open");
New=new JMenuItem("New");
save=new JMenuItem("Save");
save_as=new JMenuItem("Save As");
page_setup=new JMenuItem("Page Setup");
print=new JMenuItem("Print");
exit=new JMenuItem("Exit");
undo=new JMenuItem("Undo");
cut=new JMenuItem("Cut");
copy=new JMenuItem("Copy");
paste=new JMenuItem("Paste");
find=new JMenuItem("Find");
replace=new JMenuItem("Replace");
selectAll=new JMenuItem("selectAll");
delete=new JMenuItem("Delete");
td=new JMenuItem("Time/Date");
Goto=new JMenuItem("goto");
open.addActionListener(this);   
New.addActionListener(this);   
save.addActionListener(this); 
save_as.addActionListener(this); 
page_setup.addActionListener(this); 
print.addActionListener(this);
exit.addActionListener(this); 
undo.addActionListener(this);
cut.addActionListener(this); 
copy.addActionListener(this); 
paste.addActionListener(this);
find.addActionListener(this); 
replace.addActionListener(this); 
selectAll.addActionListener(this); 
td.addActionListener(this); 
delete.addActionListener(this); 
Goto.addActionListener(this);
/*ta.addCaretListener(new CaretListener(){
    public void caretUpdate(CaretEvent e)
    {
        int pos=0,lineNumber=0,column=0;
        try  
        {  
            pos=ta.getCaretPosition();  
            lineNumber=ta.getLineOfOffset(pos);  
            column=pos-ta.getLineStartOffset(lineNumber);  
        }catch(Exception excp){}  
        if(ta.getText().length()==0){lineNumber=0; column=0;}  
        statusBar.setText("||       Ln "+(lineNumber+1)+", Col "+(column+1));
    }
});*/
open.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,ActionEvent.CTRL_MASK));
New.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,ActionEvent.CTRL_MASK));
undo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,ActionEvent.CTRL_MASK));
save.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.CTRL_MASK));
save_as.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,ActionEvent.CTRL_MASK));
print.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,ActionEvent.CTRL_MASK));
cut.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X,ActionEvent.CTRL_MASK));
copy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK));
paste.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V,ActionEvent.CTRL_MASK));
selectAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,ActionEvent.CTRL_MASK));
td.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5,ActionEvent.CTRL_MASK));
delete.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE,ActionEvent.CTRL_MASK));
Goto.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G,ActionEvent.CTRL_MASK));
undo.setEnabled(false);
file=new JMenu("File");    
file.add(open); 
file.add(New);
file.add(save);
file.add(save_as);
file.add(sep);
file.add(page_setup);
file.add(print);
file.add(sep1);
file.add(exit);
edit=new JMenu("Edit");
edit.add(undo);
edit.add(sep2);
edit.add(cut);
edit.add(copy);
edit.add(paste);
edit.add(delete);
edit.add(sep3);
edit.add(find);
edit.add(replace);
edit.add(Goto);
edit.add(sep4);
edit.add(selectAll);
edit.add(td);
format=new JMenu("Format");
view=new JMenu("View");
help=new JMenu("Help");
mb=new JMenuBar();    
mb.setBounds(0,0,800,20);    
mb.add(file); mb.add(edit);mb.add(format);mb.add(view);mb.add(help);          
ta=new JTextArea(800,800);    
ta.setBounds(0,20,800,800); 
Image img =new ImageIcon("D:\\notepad.png").getImage();
fr.setIconImage(img);
JScrollPane sc=new JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
fr.setContentPane(sc);
fr.add(mb);    
fr.add(ta);    
fr.setSize(500,500);    
fr.setLayout(null);    
fr.setVisible(true);    
fr.setTitle("untitled -Notepad");
fr.setDefaultCloseOperation(EXIT_ON_CLOSE);  
}    
public void actionPerformed(ActionEvent e) {    
if(e.getSource()==open){      
    int i=fc.showOpenDialog(this);  
    if(i==JFileChooser.APPROVE_OPTION){    
        File f=fc.getSelectedFile();    
        String filepath=f.getPath();  
        String n=f.getName();
        try{  
        BufferedReader br=new BufferedReader(new FileReader(filepath));    
        String s1="",s2="";                         
        while((s1=br.readLine())!=null){    
        s2+=s1+"\n";    
        }    
        fr.setTitle(n+"-Notepad");
        ta.setText(s2);    
        br.close();    
        }catch (Exception ex) {ex.printStackTrace();  }                 
    }    
}
if (e.getSource()==New)
{
    ta.setText(" ");
    fr.setTitle("untitled -Notepad");
}
if(e.getSource()==save)
{
    fc.showSaveDialog(open);
    File f=fc.getSelectedFile();
    String s=f.getName();
    fr.setTitle(s+"-Notepad");
}
if(e.getSource()==save_as)
{
    File temp=null;  
    fc.setDialogTitle("Save As...");  
    fc.setApproveButtonText("Save Now");   
    fc.setApproveButtonMnemonic(KeyEvent.VK_S);  
    fc.setApproveButtonToolTipText("Click me to save!"); 
    while(true)
    {
        //FileChooserExample om=new FileChooserExample();
        temp=fc.getSelectedFile();  
        if(!temp.exists()) break;  
        if(   JOptionPane.showConfirmDialog(  
            NotepadProject.this.fr,"<html>"+temp.getPath()+" already exists.<br>Do you want to replace it?<html>",  
            "Save As",JOptionPane.YES_NO_OPTION  
            )==JOptionPane.YES_OPTION)  
        break;
    }
    fc.showSaveDialog(open);
    File f=fc.getSelectedFile();
    String s=f.getName();
    fr.setTitle(s+"-Notepad");
    
}
if(e.getSource()==exit)
{
    fr.setVisible(false);
}
if(e.getSource()==cut)
{
    ta.cut();
}
if(e.getSource()==copy)
{
    ta.copy();
}
if(e.getSource()==paste)
{
    ta.paste();
}
if(e.getSource()==selectAll)
{
    ta.selectAll();
}
if(e.getSource()==find)
{
    ta.findComponentAt(5,1);
}
if(e.getSource()==print)
{
    JOptionPane.showMessageDialog(NotepadProject.this.fr, "printer is not attached to computer","printer not found", HEIGHT);
}
if(e.getSource()==delete)
{
    ta.replaceSelection("");
}
if(e.getSource()==td)
{
    ta.insert(new Date().toString(),ta.getSelectionStart());
}
}          
public static void main(String[] args) {    
    new NotepadProject();    
               
}    
} 
