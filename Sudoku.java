/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyProjects;

import java.awt.event.*;
import javax.swing.*;

public class Sudoku implements ActionListener{
    //JTextField t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17,t18,t19,t20,t21,t82,t22,t23,t24,t25,t26,t27,t28,t29,t30,t31,t32,t33,t34,t35,t36,t37,t38,t39,t40,t41,t42,t43,t44,t45,t46,t47,t48,t49,t50,t51,t52,t53,t54,t55,t56,t57,t58,t59,t60,t61,t62,t63,t64,t65,t66,t67,t68,t69,t70,t71,t72,t73,t74,t75,t76,t77,t78,t79,t80,t81;
    JButton check;
    JTextField t82;
    JTextField[][] jt=new JTextField[9][9];
    JFrame f=new JFrame("sudoku puzzle");
    JLabel la=new JLabel("result");
    Sudoku()
    {
        t82=new JTextField("");
        la.setBounds(200,550,400,50);
        check=new JButton("Check");
        check.setBounds(200,500,100,50);
        f.add(check);
        jt[0][0]=new JTextField("5");
        jt[0][0].setBounds(0,0,50,50);
        jt[0][0].setEditable(false);
        jt[0][1]=new JTextField("3");
        jt[0][1].setBounds(50,0,50,50);
        jt[0][1].setEditable(false);
        jt[0][2]=new JTextField("");
        jt[0][2].setBounds(100,0,50,50);
        jt[0][2].setEditable(true);
        jt[0][3]=new JTextField("");
        jt[0][3].setBounds(150,0,50,50);
        jt[0][3].setEditable(true);
        jt[0][4]=new JTextField("7");
        jt[0][4].setBounds(200,0,50,50);
        jt[0][4].setEditable(false);
        jt[0][5]=new JTextField("");
        jt[0][5].setBounds(250,0,50,50);
        jt[0][5].setEditable(true);
        jt[0][6]=new JTextField("");
        jt[0][6].setBounds(300,0,50,50);
        jt[0][6].setEditable(true);
        jt[0][7]=new JTextField("");
        jt[0][7].setBounds(350,0,50,50);
        jt[0][7].setEditable(true);
        jt[0][8]=new JTextField("");
        jt[0][8].setBounds(400,0,50,50);
        jt[0][8].setEditable(true);
        jt[1][0]=new JTextField("6");
        jt[1][0].setBounds(0,50,50,50);
        jt[1][0].setEditable(false);
        jt[1][1]=new JTextField("");
        jt[1][1].setBounds(50,50,50,50);
        jt[1][1].setEditable(true);
        jt[1][2]=new JTextField("");
        jt[1][2].setBounds(100,50,50,50);
        jt[1][2].setEditable(true);
        jt[1][3]=new JTextField("1");
        jt[1][3].setBounds(150,50,50,50);
        jt[1][3].setEditable(false);
        jt[1][4]=new JTextField("9");
        jt[1][4].setBounds(200,50,50,50);
        jt[1][4].setEditable(false);
        jt[1][5]=new JTextField("5");
        jt[1][5].setBounds(250,50,50,50);
        jt[1][5].setEditable(false);
        jt[1][6]=new JTextField("");
        jt[1][6].setBounds(300,50,50,50);
        jt[1][6].setEditable(true);
        jt[1][7]=new JTextField("");
        jt[1][7].setBounds(350,50,50,50);
        jt[1][7].setEditable(true);
        jt[1][8]=new JTextField("");
        jt[1][8].setBounds(400,50,50,50);
        jt[1][8].setEditable(true);
        jt[2][0]=new JTextField("");
        jt[2][0].setBounds(0,100,50,50);
        jt[2][0].setEditable(true);
        jt[2][1]=new JTextField("9");
        jt[2][1].setBounds(50,100,50,50);
        jt[2][1].setEditable(false);
        jt[2][2]=new JTextField("8");
        jt[2][2].setBounds(100,100,50,50);
        jt[2][2].setEditable(false);
        jt[2][3]=new JTextField("");
        jt[2][3].setBounds(150,100,50,50);
        jt[2][3].setEditable(true);
        jt[2][4]=new JTextField("");
        jt[2][4].setBounds(200,100,50,50);
        jt[2][4].setEditable(true);
        jt[2][5]=new JTextField("");
        jt[2][5].setBounds(250,100,50,50);
        jt[2][5].setEditable(true);
        jt[2][6]=new JTextField("");
        jt[2][6].setBounds(300,100,50,50);
        jt[2][6].setEditable(true);
        jt[2][7]=new JTextField("6");
        jt[2][7].setBounds(350,100,50,50);
        jt[2][7].setEditable(false);
        jt[2][8]=new JTextField("");
        jt[2][8].setBounds(400,100,50,50);
        jt[2][8].setEditable(true);
        jt[3][0]=new JTextField("8");
        jt[3][0].setBounds(0,150,50,50);
        jt[3][0].setEditable(false);
        jt[3][1]=new JTextField("");
        jt[3][1].setBounds(50,150,50,50);
        jt[3][1].setEditable(true);
        jt[3][2]=new JTextField("");
        jt[3][2].setBounds(100,150,50,50);
        jt[3][2].setEditable(true);
        jt[3][3]=new JTextField("");
        jt[3][3].setBounds(150,150,50,50);
        jt[3][3].setEditable(true);
        jt[3][4]=new JTextField("6");
        jt[3][4].setBounds(200,150,50,50);
        jt[3][4].setEditable(false);
        jt[3][5]=new JTextField("");
        jt[3][5].setBounds(250,150,50,50);
        jt[3][5].setEditable(true);
        jt[3][6]=new JTextField("");
        jt[3][6].setBounds(300,150,50,50);
        jt[3][6].setEditable(true);
        jt[3][7]=new JTextField("");
        jt[3][7].setBounds(350,150,50,50);
        jt[3][7].setEditable(true);
        jt[3][8]=new JTextField("3");
        jt[3][8].setBounds(400,150,50,50);
        jt[3][8].setEditable(false);
        jt[4][0]=new JTextField("4");
        jt[4][0].setBounds(0,200,50,50);
        jt[4][0].setEditable(false);
        jt[4][1]=new JTextField("");
        jt[4][1].setBounds(50,200,50,50);
        jt[4][1].setEditable(true);
        jt[4][2]=new JTextField("");
        jt[4][2].setBounds(100,200,50,50);
        jt[4][2].setEditable(true);
        jt[4][3]=new JTextField("8");
        jt[4][3].setBounds(150,200,50,50);
        jt[4][3].setEditable(false);
        jt[4][4]=new JTextField("");
        jt[4][4].setBounds(200,200,50,50);
        jt[4][4].setEditable(true);
        jt[4][5]=new JTextField("3");
        jt[4][5].setEditable(false);
        jt[4][5].setBounds(250,200,50,50);
        jt[4][6]=new JTextField("");
        jt[4][6].setBounds(300,200,50,50);
        jt[4][6].setEditable(true);
        jt[4][7]=new JTextField("");
        jt[4][7].setBounds(350,200,50,50);
        jt[4][7].setEditable(true);
        jt[4][8]=new JTextField("1");
        jt[4][8].setBounds(400,200,50,50);
        jt[4][8].setEditable(false);
        jt[5][0]=new JTextField("7");
        jt[5][0].setBounds(0,250,50,50);
        jt[5][0].setEditable(false);
        jt[5][1]=new JTextField("");
        jt[5][1].setBounds(50,250,50,50);
        jt[5][1].setEditable(true);
        jt[5][2]=new JTextField("");
       jt[5][2].setBounds(100,250,50,50);
        jt[5][2].setEditable(true);
        jt[5][3]=new JTextField("");
        jt[5][3].setBounds(150,250,50,50);
        jt[5][3].setEditable(true);
        jt[5][4]=new JTextField("2");
        jt[5][4].setBounds(200,250,50,50);
        jt[5][4].setEditable(false);
        jt[5][5]=new JTextField("");
        jt[5][5].setBounds(250,250,50,50);
        jt[5][5].setEditable(true);
        jt[5][6]=new JTextField("");
        jt[5][6].setBounds(300,250,50,50);
        jt[5][6].setEditable(true);
        jt[5][7]=new JTextField("");
        jt[5][7].setBounds(350,250,50,50);
        jt[5][7].setEditable(true);
        jt[5][8]=new JTextField("6");
        jt[5][8].setBounds(400,250,50,50);
        jt[5][8].setEditable(false);
        jt[6][0]=new JTextField("");
        jt[6][0].setBounds(0,300,50,50);
        jt[6][0].setEditable(true);
        jt[6][1]=new JTextField("6");
        jt[6][1].setBounds(50,300,50,50);
        jt[6][1].setEditable(false);
        jt[6][2]=new JTextField("");
        jt[6][2].setBounds(100,300,50,50);
        jt[6][2].setEditable(true);
        jt[6][3]=new JTextField("");
        jt[6][3].setBounds(150,300,50,50);
        jt[6][3].setEditable(true);
        jt[6][4]=new JTextField("");
        jt[6][4].setBounds(200,300,50,50);
        jt[6][4].setEditable(true);
        jt[6][5]=new JTextField("");
        jt[6][5].setBounds(250,300,50,50);
        jt[6][5].setEditable(true);
        jt[6][6]=new JTextField("2");
        jt[6][6].setBounds(300,300,50,50);
        jt[6][6].setEditable(false);
        jt[6][7]=new JTextField("8");
        jt[6][7].setBounds(350,300,50,50);
        jt[6][7].setEditable(false);
        jt[6][8]=new JTextField("");
        jt[6][8].setBounds(400,300,50,50);
        jt[6][8].setEditable(true);
        jt[7][0]=new JTextField("");
        jt[7][0].setBounds(0,350,50,50);
        jt[7][0].setEditable(true);
        jt[7][1]=new JTextField("");
        jt[7][1].setBounds(50,350,50,50);
        jt[7][1].setEditable(true);
        jt[7][2]=new JTextField("");
        jt[7][2].setBounds(100,350,50,50);
        jt[7][2].setEditable(true);
        jt[7][3]=new JTextField("4");
        jt[7][3].setBounds(150,350,50,50);
        jt[7][3].setEditable(false);
        jt[7][4]=new JTextField("1");
        jt[7][4].setBounds(200,350,50,50);
        jt[7][4].setEditable(false);
        jt[7][5]=new JTextField("9");
        jt[7][5].setBounds(250,350,50,50);
        jt[7][5].setEditable(false);
        jt[7][6]=new JTextField("");
        jt[7][6].setBounds(300,350,50,50);
        jt[7][6].setEditable(true);
        jt[7][7]=new JTextField("");
        jt[7][7].setBounds(350,350,50,50);
        jt[7][7].setEditable(true);
        jt[7][8]=new JTextField("5");
        jt[7][8].setEditable(false);
        jt[7][8].setBounds(400,350,50,50);
        jt[8][0]=new JTextField("");
        jt[8][0].setBounds(0,400,50,50);
        jt[8][0].setEditable(true);
        jt[8][1]=new JTextField("");
        jt[8][1].setBounds(50,400,50,50);
        jt[8][1].setEditable(true);
        jt[8][2]=new JTextField("");
        jt[8][2].setBounds(100,400,50,50);
        jt[8][2].setEditable(true);
        jt[8][3]=new JTextField("");
        jt[8][3].setBounds(150,400,50,50);
        jt[8][3].setEditable(true);
        jt[8][4]=new JTextField("8");
        jt[8][4].setBounds(200,400,50,50);
        jt[8][4].setEditable(false);
        jt[8][5]=new JTextField("");
        jt[8][5].setBounds(250,400,50,50);
        jt[8][5].setEditable(true);
        jt[8][6]=new JTextField("");
        jt[8][6].setBounds(300,400,50,50);
        jt[8][6].setEditable(true);
        jt[8][7]=new JTextField("7");
        jt[8][7].setBounds(350,400,50,50);
        jt[8][7].setEditable(false);
        jt[8][8]=new JTextField("9");
        jt[8][8].setBounds(400,400,50,50);
        jt[8][8].setEditable(false);
        f.add(la);
        for(int i=0;i<9;i++)
        {
            for(int j=0;j<9;j++)
            {
                //jt[i][j].addCaretListener(this);
                f.add(jt[i][j]);
            }
        }
        f.add(t82);
        f.setSize(800,800);
        f.setVisible(true);
        f.setLayout(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        check.addActionListener(this);
    }
    public void actionPerformed(ActionEvent e)
    {
        String d;
        int m,a=0,a1=0;
        int c=0;
        for(int i=0;i<9;i++)
        {
            for(int j=0;j<9;j++)
            {
                if(jt[i][j].isEditable()==true)
                {
                    int l=0,l1=0,l2=0,l3=0;
                    d=jt[i][j].getText();
                    if(d!="")
                    {
                        a=i/3;a1=j/3;
                        int b=Integer.parseInt(d);
                        if(b>0&&b<10)
                        {
                            for(int k=0;k<9;k++)
                            {
                                if((Integer.parseInt(jt[i][k].getText())!=b))
                                {
                                    l3++;
                                }
                                if((Integer.parseInt(jt[k][j].getText())!=b))
                                l++;
                            }
                            if(a==a1)
                            {
                                for(int k1=3*a;k1<3*(a+1);k1++)
                                {
                                    for(int k2=3*a;k2<3*(a+1);k2++)
                                    {
                                        if(Integer.parseInt(jt[k1][k2].getText())!=b)
                                        {
                                            l1++;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for(int k3=3*a;k3<3*(a+1);k3++)
                                {
                                    for(int k4=(a1)*3;k4<((a1)+1)*3;k4++)
                                    {
                                        if(Integer.parseInt(jt[k3][k4].getText())!=b)
                                        {
                                            l2++;
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            la.setText("PLEASE ENTER NUMBERS BETWEEN 0-9");
                        }
                    }
                    else
                    {
                        la.setText("PLEASE ENTER NUMERICALS");
                    }
                    if((l==8&&l3==8)&&(l1==8||l2==8))
                    {
                        c=1;
                    }
                    else
                    {
                        c=3;
                        break;
                    }
                }
            }
            if(c==3)
            {
                c=2;
                break;
            }
        }
        if(c==1)
        {
            la.setText("puzzle is accepted");
        }
        else if(c==2)
        {
            la.setText("not accepted");
        }
    }
    public static void main(String[] args) {
        new Sudoku();
    }
}
